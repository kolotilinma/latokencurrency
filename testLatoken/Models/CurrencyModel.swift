//
//  CurrencyModel.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import Foundation

struct CurrencyModel: Decodable, Encodable {
    var id = ""
    var name = ""
    var tag = ""
    var description = ""
    var logo = ""
    var image = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case tag
        case description
        case logo
//        case image
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.tag = try container.decode(String.self, forKey: .tag)
        self.description = try container.decode(String.self, forKey: .description)
        self.logo = try container.decode(String.self, forKey: .logo)
//        self.image = try container.decode(String.self, forKey: .image)
    }
    
}
