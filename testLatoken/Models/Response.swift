//
//  Response.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import Foundation

struct Response: Decodable {
    var value: Double?
    var tag = ""
    
    enum CodingKeys: String, CodingKey {
        case value
        case tag
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.value = try container.decode(Double.self, forKey: .value)
    }
    
}
