//
//  CurrencyLogoModel.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import Foundation

struct CurrencyLogoModel: Decodable {
    var id = ""
    var logo = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case logo
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.logo = try container.decode(String.self, forKey: .logo)
    }
    
}
