//
//  CurrencyTableViewCell.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    var item: CurrencyModel?
    
    @IBOutlet weak var tagCurrencyLabel: UILabel!
    @IBOutlet weak var nameCurrencyLabel: UILabel!
    @IBOutlet weak var currencyImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setCell(_ item: CurrencyModel) {
        self.item = item
        
        guard self.item != nil else { return }
        
        self.tagCurrencyLabel.text = item.tag
        self.nameCurrencyLabel.text = item.name
        
        if let cachedData = CacheManager.getLogoCache(self.item!.image) {
            self.currencyImage.image = UIImage(data: cachedData)
            return
        }
        
        guard let url = URL(string: self.item!.image) else { return }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil && data != nil {
                CacheManager.setLogoCache(url.absoluteString, data)
                if url.absoluteString != self.item?.image {
                    return
                }

                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    self.currencyImage.image = image
                }
            }
        }
        dataTask.resume()
    }
    
    
}
