//
//  CurrencyHistoryTableViewCell.swift
//  testLatoken
//
//  Created by Михаил on 15.07.2020.
//

import UIKit

class CurrencyHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var tagCurrencyLabel: UILabel!
    @IBOutlet weak var valueCarrencyLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
