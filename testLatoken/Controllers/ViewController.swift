//
//  ViewController.swift
//  testLatoken
//
//  Created by Михаил on 13.07.2020.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showButton: UIButton!
    
    //MARK: - Properties
    var results: [Response] = []
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigationController()
        setupTextField()
    }
    
    //MARK: - Actions
    @objc func showHistory() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "FavoriteTableViewController") as! FavoriteTableViewController
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    @IBAction func calculateButtonPressed(_ sender: Any) {
        dismissKeyboard()
        showCurrency()
    }
    
    @IBAction func showCurrencyController(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "CurrencyTableViewController") as! CurrencyTableViewController
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - Helpers
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func setupTextField() {
        self.currencyTextField.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gesture)
    }
    
    func setupNavigationController() {
        let historyBarButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(showHistory))
        navigationItem.rightBarButtonItem = historyBarButton
    }
    
    //MARK: - API
    func showCurrency() {
        guard let userCurrency = currencyTextField.text?.uppercased() else { return }
        let url = URL(string: "\(Constants.API_URL)\(userCurrency)/USD")
        guard url != nil else { return }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil || data == nil {
                return
            }
            do {
                let decoder = JSONDecoder()
                var response = try decoder.decode(Response.self, from: data!)
                if response.value !=  nil {
                    response.tag = userCurrency
                    self.results.insert(response, at: 0)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            catch {
                
            }
        }
        dataTask.resume()
    }
    
}

extension ViewController: CurrencyTableViewControllerProtocol {
    func chooseCurrency(item: CurrencyModel) {
        currencyTextField.text = item.tag
        showButton.isEnabled = true
        showCurrency()
        navigationController?.popViewController(animated: true)
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != "" {
            showButton.isEnabled = true
        } else {
            showButton.isEnabled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyHistory", for: indexPath) as! CurrencyHistoryTableViewCell
        cell.tagCurrencyLabel.text = results[indexPath.row].tag
        cell.valueCarrencyLabel.text = "\(results[indexPath.row].value ?? 0) USD"
        return cell
    }
}
