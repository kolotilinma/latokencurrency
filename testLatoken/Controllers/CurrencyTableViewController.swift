//
//  CurrencyTableViewController.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import UIKit

protocol CurrencyTableViewControllerProtocol {
    func chooseCurrency(item: CurrencyModel)
}

class CurrencyTableViewController: UITableViewController {

    //MARK: - Properties
    var delegate: CurrencyTableViewControllerProtocol?
    var items = [CurrencyModel]()
    var filteredItems = [CurrencyModel]()
    let searchController = UISearchController(searchResultsController: nil)
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        tableView.tableFooterView = UIView()
        dowloadCurrency()
        
    }
    
    
    // MARK: - Helper
    func setupNavigationController() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        searchController.searchBar.delegate = self
    }
    
    
    // MARK: - API
    func dowloadCurrency() {
        NetworkService().dowloadCurrency { (result) in
            switch result {
            case .success(let items):
                DispatchQueue.main.async {
                    self.items = items
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredItems.count
        } else {
            return items.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath) as! CurrencyTableViewCell
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.setCell(filteredItems[indexPath.row])
        } else {
            cell.setCell(items[indexPath.row])
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item: CurrencyModel
        if !filteredItems.isEmpty {
            item = filteredItems[indexPath.row]
        } else {
            item = items[indexPath.row]
        }
        UserDefaults.standard.saveCurrency(currency: item)
        delegate?.chooseCurrency(item: item)
        
    }

}

extension CurrencyTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredItems = items.filter({ (item) -> Bool in
            return (item.tag).lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                (item.name).lowercased().contains(searchController.searchBar.text!.lowercased())
        })
        tableView.reloadData()
    }
}



