//
//  FavoriteTableViewController.swift
//  testLatoken
//
//  Created by Михаил on 15.07.2020.
//

import UIKit

class FavoriteTableViewController: UITableViewController {

    var delegate: CurrencyTableViewControllerProtocol?
    var items = UserDefaults.standard.savedFavoritesCurrency()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dowloadCurrencyLogo()
    }

    func dowloadCurrencyLogo() {
        NetworkService().dowloadCurrencyLogo(items: items) { (result) in
            switch result {
            case .success(let items):
                self.items = items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath) as! CurrencyTableViewCell
        
        cell.setCell(items[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let item = self.items[indexPath.row]
        items.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        UserDefaults.standard.deleteCurrency(currency: item)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        delegate?.chooseCurrency(item: item)
    }

}
