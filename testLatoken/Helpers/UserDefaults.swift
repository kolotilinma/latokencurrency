//
//  UserDefaults.swift
//  testLatoken
//
//  Created by Михаил on 15.07.2020.
//

import Foundation

extension UserDefaults {
    
    static let favoritedCurrencyKey = "favoritedCurrencyKey"
    
    func deleteCurrency(currency: CurrencyModel) {
        let currencyArry = savedFavoritesCurrency()
        let filteredCurrencyArry = currencyArry.filter { (item) -> Bool in
            return item.tag != currency.tag
        }
        
        do {
            let data = try JSONEncoder().encode(filteredCurrencyArry)
            UserDefaults.standard.set(data, forKey: UserDefaults.favoritedCurrencyKey)
        } catch let encodeErr {
            print("Failed to encode episode:", encodeErr)
        }
    }
    
    func saveCurrency(currency: CurrencyModel) {
        print("save \(currency.tag) with image link: \(currency.image)")
        do {
            var currencyArry = savedFavoritesCurrency()
            currencyArry.insert(currency, at: 0)
            let data = try JSONEncoder().encode(currencyArry)
            UserDefaults.standard.set(data, forKey: UserDefaults.favoritedCurrencyKey)
            
        } catch let encodeErr {
            print("Failed to encode currency:", encodeErr)
        }
    }
    
    func savedFavoritesCurrency() -> [CurrencyModel] {
        guard let currencyArryData = data(forKey: UserDefaults.favoritedCurrencyKey) else { return [] }
        
        do {
            let currencyArry = try JSONDecoder().decode([CurrencyModel].self, from: currencyArryData)
            return currencyArry
        } catch let decodeErr {
            print("Failed to currencyArry:", decodeErr)
        }
        
        return []
    }
    
}
