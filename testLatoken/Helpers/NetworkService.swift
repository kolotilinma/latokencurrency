//
//  NetworkService.swift
//  testLatoken
//
//  Created by Михаил on 15.07.2020.
//

import Foundation

class NetworkService {
    
    func dowloadCurrency(completion: @escaping (_ items: (Result<[CurrencyModel], Error>)) -> Void) {
        guard let url = URL(string: Constants.API_URL_CURRENCY) else { return }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error != nil || data == nil {
                completion(.failure(error!))
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode([CurrencyModel].self, from: data!)
                if !response.isEmpty {
                    self.dowloadCurrencyLogo(items: response) { (result) in
                        switch result {
                        case .success(let items):
                            completion(.success(items))
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                }
            }
            catch let error {
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
    
    func dowloadCurrencyLogo(items: [CurrencyModel], completion: @escaping (_ items: (Result<[CurrencyModel], Error>)) -> Void) {
        var items = items
        guard let url = URL(string: Constants.API_URL_CURRENCY_LOGO) else { return }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error != nil || data == nil {
                completion(.failure(error!))
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode([CurrencyLogoModel].self, from: data!)
                if !response.isEmpty {
                    for i in 0..<(items.count) {
                        let id = items[i].id
                        if let responsId = response.firstIndex(where: {$0.id == id}) {
                            items[i].image = response[responsId].logo
                        }
                    }
                    completion(.success(items))
                }
            }
            catch {
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
    
}
