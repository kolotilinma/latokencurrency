//
//  CacheManager.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import Foundation

class CacheManager {
    static var cache = [String: Data]()
    
    static func setLogoCache(_ url: String, _ data: Data?) {
        cache[url] = data
    }
    
    static func getLogoCache(_ url: String) -> Data? {
        return cache[url]
    }
}
