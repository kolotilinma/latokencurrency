//
//  Constants.swift
//  testLatoken
//
//  Created by Михаил on 14.07.2020.
//

import Foundation

struct Constants {
    static var API_KEY = ""
    
    static var API_URL = "https://api.latoken.com/api/v2/rate/" // BTC/USD "
    static var API_URL_CURRENCY = "https://api.latoken.com/api/v2/currency"
    static var API_URL_CURRENCY_LOGO = "https://cms.latoken.com/api/currency/v1/info/"
    
}
